package Threading;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable{
    private  BlockingQueue<String> sharedQueue;
    private int size = 4;
    Logger logger = LoggerFactory.getLogger(PCExample.class);
    volatile int i=0;

    public Producer(BlockingQueue<String> sharedQueue, int size){
        this.sharedQueue = sharedQueue;
        this.size = size;
    }


    @Override
    public void run() {
        for ( i=0; i<1000;i++){

            try{

                synchronized (sharedQueue){

                    produce(Thread.currentThread().getName()+"_"+i);
                    logger.debug("Produced: " + Thread.currentThread().getName()+"_"+i);
                }

            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println("test");
    }

    public void produce(String th_i) throws InterruptedException {

        while ( sharedQueue.size() >= size){
            synchronized (sharedQueue){
                logger.debug("Queue is full "+ Thread.currentThread().getName()+"is waiting, size:"+sharedQueue.size());
                sharedQueue.wait();
            }


        }
        synchronized (sharedQueue){
            sharedQueue.add(th_i);
            sharedQueue.notifyAll();
        }

    }




}
