package Threading;

import java.util.*;
import java.util.concurrent.*;
import org.slf4j.*;

//Simple Producer Consumer Problem
public class PCExample {


    public static void main(String[] args) throws Exception{
        Logger logger = LoggerFactory.getLogger(PCExample.class);

        BlockingQueue<String> sharedQueue= new LinkedBlockingDeque<>();
        int size = 8;
        Thread prodThread_1 = new Thread(new Producer(sharedQueue, size),"Producer_1");
        Thread prodThread_2 = new Thread(new Producer(sharedQueue, size),"Producer_2");
        Thread prodThread_3 = new Thread(new Producer(sharedQueue, size),"Producer_3");
        Thread prodThread_4 = new Thread(new Producer(sharedQueue, size),"Producer_4");

        prodThread_1.start();
        prodThread_2.start();
        prodThread_3.start();
        prodThread_4.start();

        TimerTask timerTask = new Consumer(sharedQueue, size);
        Timer timer = new Timer(true);
        timer.schedule(timerTask, 0);

        prodThread_1.join(2000);
        prodThread_2.join(2000);
        prodThread_3.join(2000);
        prodThread_4.join(2000);

        Thread.sleep(20000);
        timer.cancel();

        logger.debug("finished all thread");
    }

}
