package Threading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.*;


public class Consumer extends TimerTask{
    private  BlockingQueue<String> sharedQueue;
    private  int size = 4;
    Logger logger = LoggerFactory.getLogger(PCExample.class);

    public Consumer(BlockingQueue<String> sharedQueue, int size){
        this.sharedQueue = sharedQueue;
        this.size = size;
    }

    @Override
    public void run() {
        try{
            while(!Thread.interrupted()) {
                synchronized (sharedQueue){
                    logger.debug("Consume thread="+ Thread.currentThread().getName()+ ":"+ consume());

                }

                //Thread.sleep(50);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("test");
    }

    public String consume() throws InterruptedException{
        while(sharedQueue.isEmpty()){
            synchronized (sharedQueue){
                logger.debug("Queue is empty"+ Thread.currentThread().getName()+"is waiting size:"+ sharedQueue.size());
                sharedQueue.wait();
            }

        }
        synchronized (sharedQueue){
            sharedQueue.notifyAll();
            return (String) sharedQueue.remove();
        }


    }
}
